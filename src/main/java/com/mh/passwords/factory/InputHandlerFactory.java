package com.mh.passwords.factory;


import com.mh.passwords.chainofresponsibility.InputHandler;

import java.io.PrintStream;

public interface InputHandlerFactory {
    InputHandler createInputHandler(PrintStream printStream);
}
