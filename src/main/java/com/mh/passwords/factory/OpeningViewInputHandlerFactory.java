package com.mh.passwords.factory;

import com.mh.passwords.chainofresponsibility.ExitHandler;
import com.mh.passwords.chainofresponsibility.HelpHandler;
import com.mh.passwords.chainofresponsibility.InputHandler;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;

public class OpeningViewInputHandlerFactory implements InputHandlerFactory {

    @Override
    public InputHandler createInputHandler(PrintStream printStream) {
        List<InputHandler> inputHandlers = Arrays.asList(
                new ExitHandler(printStream),
                new HelpHandler(printStream)
        );

        for (int i = inputHandlers.size() - 2; i >= 0; --i)
            inputHandlers.get(i).setNextHandler(inputHandlers.get(i + 1));

        return inputHandlers.get(0);
    }
}
