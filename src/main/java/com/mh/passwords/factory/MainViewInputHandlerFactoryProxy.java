package com.mh.passwords.factory;

import com.mh.passwords.authentication.Authentication;
import com.mh.passwords.chainofresponsibility.*;
import com.mh.passwords.service.PasswordGeneratorImpl;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;

public class MainViewInputHandlerFactoryProxy implements InputHandlerFactory {
    @Override
    public InputHandler createInputHandler(PrintStream printStream) {
        if (Authentication.getInstance().isAuthenticated())
            return new MainViewInputHandlerFactory().createInputHandler(printStream);
        else {
            List<InputHandler> inputHandlers = Arrays.asList(
                    new ExitHandler(printStream),
                    new GenerateHandler(printStream, new PasswordGeneratorImpl()),
                    new HelpHandler(printStream),
                    new GoBackHandler(printStream)
            );

            for (int i = inputHandlers.size() - 2; i >= 0; --i)
                inputHandlers.get(i).setNextHandler(inputHandlers.get(i + 1));

            return inputHandlers.get(0);
        }
    }
}
