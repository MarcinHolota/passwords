package com.mh.passwords.factory;


import com.mh.passwords.chainofresponsibility.*;
import com.mh.passwords.dao.CredentialsDAOImpl;
import com.mh.passwords.service.PasswordGeneratorImpl;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;

public class MainViewInputHandlerFactory implements InputHandlerFactory {

    @Override
    public InputHandler createInputHandler(PrintStream printStream) {

        CredentialsDAOImpl credentialsDAO = new CredentialsDAOImpl();

        List<InputHandler> inputHandlers = Arrays.asList(
                new DeleteCredentialsHandler(printStream, credentialsDAO),
                new ExitHandler(printStream),
                new GenerateHandler(printStream, new PasswordGeneratorImpl()),
                new HelpHandler(printStream),
                new LogoutHandler(printStream),
                new SaveHandler(printStream, credentialsDAO),
                new ShowAllHandler(printStream, credentialsDAO),
                new ShowSpecificHandler(printStream, credentialsDAO)
        );

        for (int i = inputHandlers.size() - 2; i >= 0; --i)
            inputHandlers.get(i).setNextHandler(inputHandlers.get(i + 1));

        return inputHandlers.get(0);
    }
}
