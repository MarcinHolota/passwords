package com.mh.passwords;

import com.mh.passwords.controller.OpeningViewController;
import com.mh.passwords.controller.ViewManager;
import com.mh.passwords.view.OpeningView;

public class PasswordsApplication {

    public static void main(String[] args) {
        ViewManager viewManager = new ViewManager();

        viewManager.addView(new OpeningView(viewManager, new OpeningViewController(viewManager)));

        while (viewManager.isNotEmpty()) {
            viewManager.getCurrentView().display();
        }
    }
}
