package com.mh.passwords.chainofresponsibility;

import com.mh.passwords.authentication.Authentication;
import com.mh.passwords.authentication.AuthenticationException;
import com.mh.passwords.dao.CredentialsDAO;
import com.mh.passwords.facade.StringBuilderFacade;
import com.mh.passwords.model.Credentials;
import com.mh.passwords.model.Response;
import com.mh.passwords.model.User;
import lombok.extern.slf4j.Slf4j;

import java.io.PrintStream;
import java.util.Optional;

@Slf4j
public class DeleteCredentialsHandler extends InputHandler {

    private final CredentialsDAO credentialsDAO;
    private final String regex = "delete credentials for .+";

    public DeleteCredentialsHandler(PrintStream printStream, CredentialsDAO credentialsDAO) {
        super(printStream);
        this.credentialsDAO = credentialsDAO;
    }

    @Override
    protected boolean canHandle(String preparedRequest) {
        return preparedRequest.matches(regex);
    }

    @Override
    protected Response handle(String preparedRequest) {

        String website = preparedRequest.substring(regex.length() - 2);

        try {
            User authenticatedUser = Authentication.getInstance().getAuthenticatedUser();

            Optional<Credentials> optionalCredentials = credentialsDAO.getCredentialsByUserAndWebsite(authenticatedUser, website);

            return optionalCredentials.map(credentials -> {
                credentialsDAO.delete(credentials);
                return new Response("Credentials: " + credentials + "\nhas been removed", Response.Status.OK);
            }).orElse(new Response("You did not have account on " + website, Response.Status.FAILED));
        } catch (AuthenticationException e) {
            log.error(e.getMessage());
            return new Response("Unable to delete credentials.", Response.Status.ERROR);
        }
    }

    @Override
    public void getHelp() {
        printStream.println(
                StringBuilderFacade.createStringBuilderFacade()
                        .appendLine("*******************************")
                        .appendLine("** How to delete credentials **")
                        .appendLine("Enter \"delete credentials for (website name)\" in command line")
                        .appendLine("For example: \"delete credentials for xyz.com\"")
                        .appendLine("*******************************")
                        .toString()
        );
        nextHandler.getHelp();
    }
}
