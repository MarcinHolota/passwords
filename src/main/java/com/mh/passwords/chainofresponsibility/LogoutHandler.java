package com.mh.passwords.chainofresponsibility;

import com.mh.passwords.authentication.Authentication;
import com.mh.passwords.authentication.AuthenticationException;
import com.mh.passwords.facade.StringBuilderFacade;
import com.mh.passwords.model.Response;
import lombok.extern.slf4j.Slf4j;

import java.io.PrintStream;

@Slf4j
public class LogoutHandler extends InputHandler {

    public LogoutHandler(PrintStream printStream) {
        super(printStream);
    }

    @Override
    protected boolean canHandle(String preparedRequest) {
        return preparedRequest.equals("logout");
    }

    @Override
    protected Response handle(String preparedRequest) {
        try {
            Authentication.getInstance().unauthenticate();

            return new Response("LOGOUT", Response.Status.LOGOUT);
        } catch (AuthenticationException e) {
            log.error(e.getMessage());
            return new Response("Unable to logout!", Response.Status.ERROR);
        }
    }

    @Override
    public void getHelp() {
        printStream.println(
                StringBuilderFacade.createStringBuilderFacade()
                        .appendLine("*******************")
                        .appendLine("** How to logout **")
                        .appendLine("Enter \"logout\" in command line")
                        .appendLine("*******************")
                        .toString()
        );
        nextHandler.getHelp();
    }
}
