package com.mh.passwords.chainofresponsibility;

import com.mh.passwords.authentication.Authentication;
import com.mh.passwords.authentication.AuthenticationException;
import com.mh.passwords.dao.CredentialsDAO;
import com.mh.passwords.model.Credentials;
import com.mh.passwords.model.Response;
import com.mh.passwords.facade.StringBuilderFacade;
import com.mh.passwords.model.User;
import lombok.extern.slf4j.Slf4j;

import java.io.PrintStream;
import java.util.Scanner;

@Slf4j
public class SaveHandler extends InputHandler {

    private final CredentialsDAO credentialsDAO;

    public SaveHandler(PrintStream printStream, CredentialsDAO credentialsDAO) {
        super(printStream);
        this.credentialsDAO = credentialsDAO;
    }

    @Override
    protected boolean canHandle(String preparedRequest) {
        return preparedRequest.equals("save credentials");
    }

    @Override
    protected Response handle(String preparedRequest) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Website: ");
        String website = scanner.nextLine();
        System.out.print("login: ");
        String login = scanner.nextLine();
        System.out.print("Password: ");
        String password = scanner.nextLine();

        try {

            User authenticatedUser = Authentication.getInstance().getAuthenticatedUser();

            Credentials credentialsToSave = credentialsDAO.getCredentialsByUserAndWebsite(authenticatedUser, website)
                    .map(credentials -> {
                        credentials.setLogin(login);
                        credentials.setPassword(password);

                        return credentials;
                    }).orElse(Credentials.builder()
                            .website(website)
                            .login(login)
                            .password(password)
                            .user(authenticatedUser)
                            .build()
                    );

            credentialsDAO.saveOrUpdate(credentialsToSave);

            return new Response("Credentials have been saved.", Response.Status.OK);
        } catch (AuthenticationException e) {
            log.error(e.getMessage());
            return new Response("Unable to save credentials.", Response.Status.ERROR);
        }
    }

    @Override
    public void getHelp() {
        printStream.println(
                StringBuilderFacade.createStringBuilderFacade()
                        .appendLine("*****************************")
                        .appendLine("** How to save credentials **")
                        .appendLine("To save credentials enter: \"save credentials\" and then you will have to fill form")
                        .appendLine("For example:")
                        .appendLine("\"save credentials")
                        .appendLine("website: xyz.com")
                        .appendLine("login: user144324")
                        .appendLine("password: d1ff1culTP455w0rd!\"")
                        .appendLine("*****************************")
                        .toString()
        );
        nextHandler.getHelp();
    }
}
