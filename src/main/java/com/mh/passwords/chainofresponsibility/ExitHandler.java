package com.mh.passwords.chainofresponsibility;

import com.mh.passwords.model.Response;
import com.mh.passwords.facade.StringBuilderFacade;

import java.io.PrintStream;

public class ExitHandler extends InputHandler {

    public ExitHandler(PrintStream printStream) {
        super(printStream);
    }

    @Override
    protected boolean canHandle(String preparedRequest) {
        return preparedRequest.equals("exit");
    }

    @Override
    protected Response handle(String preparedRequest) {
        return new Response("EXIT", Response.Status.EXIT);
    }

    @Override
    public void getHelp() {
        printStream.println(
                StringBuilderFacade.createStringBuilderFacade()
                        .appendLine("*****************")
                        .appendLine("** How to exit **")
                        .appendLine("Enter \"exit\" in command line")
                        .appendLine("*****************")
                        .toString()
        );
        nextHandler.getHelp();
    }
}
