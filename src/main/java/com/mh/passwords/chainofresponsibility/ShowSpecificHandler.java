package com.mh.passwords.chainofresponsibility;

import com.mh.passwords.authentication.Authentication;
import com.mh.passwords.authentication.AuthenticationException;
import com.mh.passwords.dao.CredentialsDAO;
import com.mh.passwords.model.Credentials;
import com.mh.passwords.model.Response;
import com.mh.passwords.model.User;
import com.mh.passwords.facade.StringBuilderFacade;
import lombok.extern.slf4j.Slf4j;

import java.io.PrintStream;
import java.util.Optional;

@Slf4j
public class ShowSpecificHandler extends InputHandler {

    private final CredentialsDAO credentialsDAO;
    private final String regex = "show credentials for .+";

    public ShowSpecificHandler(PrintStream printStream, CredentialsDAO credentialsDAO) {
        super(printStream);
        this.credentialsDAO = credentialsDAO;
    }

    @Override
    protected boolean canHandle(String preparedRequest) {
        return preparedRequest.matches(regex);
    }

    @Override
    protected Response handle(String preparedRequest) {

        String website = preparedRequest.substring(regex.length() - 2);

        try {
            User authenticatedUser = Authentication.getInstance().getAuthenticatedUser();

            Optional<Credentials> optionalCredentials = credentialsDAO.getCredentialsByUser(authenticatedUser).stream()
                    .filter(credentials -> credentials.getWebsite().equals(website))
                    .findAny();

            return optionalCredentials
                    .map(credentials -> new Response(credentials.toString(), Response.Status.OK))
                    .orElseGet(() -> new Response("You do not have an account on that website.", Response.Status.FAILED));

        } catch (AuthenticationException e) {
            log.error(e.getMessage());
            return new Response("Unable to get credentials", Response.Status.FAILED);
        }
    }

    @Override
    public void getHelp() {
        printStream.println(
                StringBuilderFacade.createStringBuilderFacade()
                        .appendLine("********************************************************")
                        .appendLine("** How to show saved credentials for specific website **")
                        .appendLine("Enter \"show credentials for (website name)\"")
                        .appendLine("For example: \"show credentials for xyz.com\"")
                        .appendLine("********************************************************")
                        .toString()
        );
        nextHandler.getHelp();
    }
}
