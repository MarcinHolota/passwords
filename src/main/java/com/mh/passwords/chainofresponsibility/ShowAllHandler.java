package com.mh.passwords.chainofresponsibility;

import com.mh.passwords.authentication.Authentication;
import com.mh.passwords.authentication.AuthenticationException;
import com.mh.passwords.dao.CredentialsDAO;
import com.mh.passwords.model.Credentials;
import com.mh.passwords.model.Response;
import com.mh.passwords.model.User;
import com.mh.passwords.facade.StringBuilderFacade;
import lombok.extern.slf4j.Slf4j;

import java.io.PrintStream;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class ShowAllHandler extends InputHandler {

    private final CredentialsDAO credentialsDAO;

    public ShowAllHandler(PrintStream printStream, CredentialsDAO credentialsDAO) {
        super(printStream);
        this.credentialsDAO = credentialsDAO;
    }

    @Override
    protected boolean canHandle(String preparedRequest) {
        return preparedRequest.equals("show credentials all");
    }

    @Override
    protected Response handle(String preparedRequest) {
        try {
            User authenticatedUser = Authentication.getInstance().getAuthenticatedUser();
            List<Credentials> credentials = credentialsDAO.getCredentialsByUser(authenticatedUser);

            String message = StringBuilderFacade.createStringBuilderFacade()
                    .appendLine("All " + authenticatedUser.getUsername() + "'s credentials:")
                    .appendLine()
                    .append(credentials.stream()
                            .map(Credentials::toString)
                            .collect(Collectors.joining("\n\n"))
                    ).toString();

            return new Response(message, Response.Status.OK);
        } catch (AuthenticationException e) {
            log.error(e.getMessage());
            return new Response("Unable to show all credentials.", Response.Status.FAILED);
        }
    }

    @Override
    public void getHelp() {
        printStream.println(
                StringBuilderFacade.createStringBuilderFacade()
                        .appendLine("***************************************")
                        .appendLine("** How to show all saved credentials **")
                        .appendLine("Enter \"show credentials all\" in command line")
                        .appendLine("***************************************")
                        .toString()
        );
        nextHandler.getHelp();
    }
}
