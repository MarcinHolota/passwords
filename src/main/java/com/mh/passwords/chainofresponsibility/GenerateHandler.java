package com.mh.passwords.chainofresponsibility;

import com.mh.passwords.model.PasswordConfiguration;
import com.mh.passwords.model.Response;
import com.mh.passwords.service.PasswordGenerator;
import com.mh.passwords.facade.StringBuilderFacade;

import java.io.PrintStream;

public class GenerateHandler extends InputHandler {

    private final PasswordGenerator passwordGenerator;

    public GenerateHandler(PrintStream printStream, PasswordGenerator passwordGenerator) {
        super(printStream);
        this.passwordGenerator = passwordGenerator;
    }

    @Override
    public boolean canHandle(String preparedRequest) {
        return preparedRequest.matches("generate password with characters ((\\s*(lower|upper|digit|special))((,\\s*|\\s+)(lower|upper|digit|special))*) \\s*with length\\s*([sml])");
    }

    @Override
    protected Response handle(String preparedRequest) {
        PasswordConfiguration passwordConfiguration = PasswordConfiguration.builder()
                .useLowerCaseCharacters(preparedRequest.contains("lower"))
                .useUpperCaseCharacters(preparedRequest.contains("upper"))
                .useDigits(preparedRequest.contains("digits"))
                .useSpecialCharacters(preparedRequest.contains("special"))
                .length(PasswordConfiguration.PasswordLength.getEnumOject(preparedRequest.substring(preparedRequest.length() - 1)))
                .build();

        return new Response("Password generated: " + passwordGenerator.generatePassword(passwordConfiguration), Response.Status.OK);
    }

    @Override
    public void getHelp() {
        printStream.println(
                StringBuilderFacade.createStringBuilderFacade()
                        .appendLine("**********************************")
                        .appendLine("** How to generate new password **")
                        .appendLine("To generate password enter: \"generate password with characters (lower, upper, digit or special) with length (s, m or l)\"")
                        .appendLine("For example: \"generate password with characters lower, digit, special with length m\"")
                        .appendLine("**********************************")
                        .toString()
        );
        nextHandler.getHelp();
    }
}
