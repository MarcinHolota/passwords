package com.mh.passwords.chainofresponsibility;

import com.mh.passwords.model.Response;
import com.mh.passwords.facade.StringBuilderFacade;

import java.io.PrintStream;

public class HelpHandler extends InputHandler {

    public HelpHandler(PrintStream printStream) {
        super(printStream);
    }

    @Override
    protected boolean canHandle(String preparedRequest) {
        return preparedRequest.equals("help");
    }

    @Override
    protected Response handle(String preparedRequest) {
        return new Response("HELP", Response.Status.HELP);
    }

    @Override
    public void getHelp() {
        printStream.println(
                StringBuilderFacade.createStringBuilderFacade()
                        .appendLine("*********************")
                        .appendLine("** How to get help **")
                        .appendLine("Enter \"help\" in command line")
                        .appendLine("*********************")
                        .toString()
        );
        nextHandler.getHelp();
    }
}
