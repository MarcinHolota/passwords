package com.mh.passwords.chainofresponsibility;

import com.mh.passwords.model.Response;

public class NilInputHandler extends InputHandler {

    private static NilInputHandler instance;

    private NilInputHandler() {
        super(null, null);
    }

    public static NilInputHandler getInstance() {
        if (instance == null) {
            instance = new NilInputHandler();
        }

        return instance;
    }

    @Override
    protected boolean canHandle(String preparedRequest) {
        return true;
    }

    @Override
    protected Response handle(String preparedRequest) {
        return new Response("** NOT RECOGNIZED REQUEST **", Response.Status.REQUEST_NOT_RECOGNIZED);
    }

    @Override
    public void getHelp() {
    }
}
