package com.mh.passwords.chainofresponsibility;

import com.mh.passwords.facade.StringBuilderFacade;
import com.mh.passwords.model.Response;

import java.io.PrintStream;

public class GoBackHandler extends InputHandler {

    public GoBackHandler(PrintStream printStream) {
        super(printStream);
    }

    @Override
    protected boolean canHandle(String preparedRequest) {
        return preparedRequest.equals("go back");
    }

    @Override
    protected Response handle(String preparedRequest) {
        return new Response("GO BACK", Response.Status.GO_BACK);
    }

    @Override
    public void getHelp() {
        printStream.println(
                StringBuilderFacade.createStringBuilderFacade()
                        .appendLine("*****************")
                        .appendLine("** How to go back **")
                        .appendLine("Enter \"go back\" in command line")
                        .appendLine("*****************")
                        .toString()
        );
        nextHandler.getHelp();
    }
}
