package com.mh.passwords.chainofresponsibility;

import com.mh.passwords.model.Response;

import java.io.PrintStream;

public abstract class InputHandler {

    protected InputHandler nextHandler;
    protected final PrintStream printStream;

    protected InputHandler(PrintStream printStream) {
        this.printStream = printStream;
        this.nextHandler = NilInputHandler.getInstance();
    }

    protected InputHandler(InputHandler nextHandler, PrintStream printStream) {
        this.nextHandler = nextHandler;
        this.printStream = printStream;
    }

    public void setNextHandler(InputHandler nextHandler) {
        this.nextHandler = nextHandler;
    }

    public Response tryHandle(String request) {
        String preparedRequest = prepareRequest(request);

        if (canHandle(preparedRequest)) {
            return handle(preparedRequest);
        } else {
            return nextHandler.tryHandle(preparedRequest);
        }
    }

    public abstract void getHelp();

    private String prepareRequest(String request) {
        return request.trim().toLowerCase();
    }

    protected abstract boolean canHandle(String preparedRequest);

    protected abstract Response handle(String preparedRequest);
}
