package com.mh.passwords.controller;

import com.mh.passwords.chainofresponsibility.InputHandler;
import com.mh.passwords.dao.UserDAOImpl;
import com.mh.passwords.factory.MainViewInputHandlerFactoryProxy;
import com.mh.passwords.factory.OpeningViewInputHandlerFactory;
import com.mh.passwords.model.Response;
import com.mh.passwords.view.LoginView;
import com.mh.passwords.view.MainView;
import com.mh.passwords.view.RegisterView;

public class OpeningViewController {

    private final ViewManager viewManager;
    private final InputHandler inputHandler;

    public OpeningViewController(ViewManager viewManager) {
        this.viewManager = viewManager;
        this.inputHandler = new OpeningViewInputHandlerFactory().createInputHandler(System.out);
    }

    public boolean handleAccountQuestionAnswer(String answer) {
        try {
            switch (Integer.parseInt(answer)) {
                case 1:
                    viewManager.addView(new LoginView(viewManager, new LoginViewController(viewManager)));
                    break;
                case 2:
                    viewManager.addView(new RegisterView(viewManager, new RegisterViewController(viewManager, new UserDAOImpl())));
                    break;
                case 3:
                    viewManager.addView(new MainView(viewManager, new MainViewController(viewManager, new MainViewInputHandlerFactoryProxy().createInputHandler(System.out))));
                    break;
                default:
                    return false;
            }
        } catch (NumberFormatException e) {
            Response response = inputHandler.tryHandle(answer);

            switch (response.getStatus()) {
                case HELP:
                    inputHandler.getHelp();
                    return false;
                case EXIT:
                    viewManager.close();
                    return true;
                case REQUEST_NOT_RECOGNIZED:
                    System.out.println(response);
                    return false;
            }
        }

        return true;
    }
}
