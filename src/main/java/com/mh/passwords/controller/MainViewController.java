package com.mh.passwords.controller;

import com.mh.passwords.chainofresponsibility.InputHandler;
import com.mh.passwords.model.Response;

public class MainViewController extends ViewController {

    private final InputHandler inputHandler;

    public MainViewController(ViewManager viewManager, InputHandler inputHandler) {
        super(viewManager);
        this.inputHandler = inputHandler;
    }

    public Response handleRequest(String request) {
        Response response = inputHandler.tryHandle(request);

        switch (response.getStatus()) {
            case LOGOUT:
            case GO_BACK:
                viewManager.popView();
                break;
            case HELP:
                inputHandler.getHelp();
                break;
            case EXIT:
                viewManager.close();
                break;
        }

        return response;
    }
}
