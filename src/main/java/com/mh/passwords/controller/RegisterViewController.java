package com.mh.passwords.controller;

import com.mh.passwords.authentication.Authentication;
import com.mh.passwords.authentication.AuthenticationException;
import com.mh.passwords.dao.UserDAO;
import com.mh.passwords.factory.MainViewInputHandlerFactoryProxy;
import com.mh.passwords.model.Response;
import com.mh.passwords.model.User;
import com.mh.passwords.view.MainView;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;

@Slf4j
public class RegisterViewController extends ViewController {

    private final UserDAO userDAO;

    public RegisterViewController(ViewManager viewManager, UserDAO userDAO) {
        super(viewManager);
        this.userDAO = userDAO;
    }

    public Response handleRegistration(String username, String password, String confirmedPassword) {
        Optional<User> userOptional = userDAO.getUserByUsername(username);

        if (userOptional.isPresent()) {
            return new Response("User with that username already exists.", Response.Status.FAILED);
        } else if (password.equals(confirmedPassword)) {
            try {
                Authentication.getInstance().registerAndAuthenticate(new User(username, password));
                viewManager.replaceView(new MainView(viewManager, new MainViewController(viewManager, new MainViewInputHandlerFactoryProxy().createInputHandler(System.out))));
                return new Response("You have been registered!", Response.Status.OK);
            } catch (AuthenticationException e) {
                log.error(e.getMessage());
                return new Response("Registration failed!", Response.Status.ERROR);
            }
        } else {
            return new Response("Passwords do not match!", Response.Status.FAILED);
        }
    }

    public Response tryAgain(String answer) {
        String preparedAnswer = answer.trim().toLowerCase();

        switch (preparedAnswer) {
            case "yes":
                return new Response("You chose to try to register again", Response.Status.OK);
            case "no":
                viewManager.popView();
                return new Response("You chose to go back", Response.Status.GO_BACK);
            default:
                return new Response("Not recognized answer", Response.Status.FAILED);
        }
    }
}
