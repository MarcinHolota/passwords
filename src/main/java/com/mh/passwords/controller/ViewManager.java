package com.mh.passwords.controller;

import com.mh.passwords.view.View;

import java.util.Stack;

public class ViewManager {
    private Stack<View> views;

    public ViewManager() {
        this.views = new Stack<>();
    }

    public ViewManager addView(View view) {
        views.push(view);

        return this;
    }

    public ViewManager replaceView(View view) {
        views.pop();
        views.push(view);

        return this;
    }

    public ViewManager popView() {
        views.pop();

        return this;
    }

    public void close() {
        views.clear();
    }

    public View getCurrentView() {
        return views.peek();
    }

    public boolean isNotEmpty() {
        return !views.isEmpty();
    }
}
