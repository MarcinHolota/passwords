package com.mh.passwords.controller;

import com.mh.passwords.authentication.Authentication;
import com.mh.passwords.authentication.AuthenticationException;
import com.mh.passwords.factory.MainViewInputHandlerFactoryProxy;
import com.mh.passwords.model.Response;
import com.mh.passwords.view.MainView;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LoginViewController extends ViewController {

    public LoginViewController(ViewManager viewManager) {
        super(viewManager);
    }

    public boolean tryToAuthenticate(String username, String password) {
        try {
            boolean isAuthenticated = Authentication.getInstance().authenticate(username, password);

            if (isAuthenticated)
                viewManager.replaceView(new MainView(viewManager, new MainViewController(viewManager, new MainViewInputHandlerFactoryProxy().createInputHandler(System.out))));

            return isAuthenticated;
        } catch (AuthenticationException e) {
            log.error(e.getMessage());
            return false;
        }
    }

    public Response tryAgain(String answer) {
        String preparedAnswer = answer.trim().toLowerCase();

        switch (preparedAnswer) {
            case "yes":
                return new Response("You chose to try to login again", Response.Status.OK);
            case "no":
                viewManager.popView();
                return new Response("You chose to go back", Response.Status.GO_BACK);
            default:
                return new Response("Not recognized answer", Response.Status.FAILED);
        }
    }
}
