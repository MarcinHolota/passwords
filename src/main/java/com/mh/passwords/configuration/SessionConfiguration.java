package com.mh.passwords.configuration;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionConfiguration {
    private static SessionConfiguration instance;
    private SessionFactory sessionFactory;

    public static SessionConfiguration getInstance() {
        if (instance == null)
            instance = new SessionConfiguration();

        return instance;
    }

    private SessionConfiguration() {
        sessionFactory = new Configuration()
                .configure()
                .buildSessionFactory();
    }

    public Session openSession() {
        return sessionFactory.openSession();
    }
}
