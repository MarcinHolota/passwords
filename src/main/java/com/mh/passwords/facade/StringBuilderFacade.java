package com.mh.passwords.facade;

public class StringBuilderFacade {
    private final StringBuilder stringBuilder;


    private StringBuilderFacade() {
        this.stringBuilder = new StringBuilder();
    }

    public static StringBuilderFacade createStringBuilderFacade() {
        return new StringBuilderFacade();
    }

    public StringBuilderFacade appendLine() {
        stringBuilder.append("\n");

        return this;
    }

    public StringBuilderFacade appendLine(String line) {
        stringBuilder.append(line).append("\n");

        return this;
    }

    public StringBuilderFacade append(String string) {
        stringBuilder.append(string);

        return this;
    }

    @Override
    public String toString() {
        return stringBuilder.toString();
    }
}
