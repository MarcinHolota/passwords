package com.mh.passwords.facade;

import com.mh.passwords.configuration.SessionConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
public class QueryFacade {
    private String query;
    private Session session;
    private Transaction transaction;
    private Map<String, Object> parameters;

    public static QueryFacade createQuery() {
        return new QueryFacade();
    }

    private QueryFacade() {
        this.session = SessionConfiguration.getInstance().openSession();
        this.parameters = new HashMap<>();
    }

    public QueryFacade withTransaction() {
        this.transaction = session.getTransaction();

        return this;
    }

    public QueryFacade withQuery(String query) {
        this.query = query;

        return this;
    }

    public QueryFacade withParameter(String name, Object parameter) {
        parameters.put(name, parameter);

        return this;
    }

    public <T> Optional<T> getSingleResult(Class<T> type) {
        if (transaction != null) {
            transaction.begin();
        }

        T result = null;

        try {
            result = prepareQuery(type).getSingleResult();
        } catch (NoResultException e) {
            log.info("Result for {} is empty", query);
        }

        if (transaction != null) {
            transaction.commit();
        }

        session.close();

        return Optional.ofNullable(result);
    }

    public <T> List<T> getList(Class<T> type) {
        if (transaction != null) {
            transaction.begin();
        }

        List<T> result = prepareQuery(type)
                .getResultList();

        if (transaction != null) {
            transaction.commit();
        }

        session.close();

        return result;
    }

    public <T> void saveOrUpdate(T object) {
        if (transaction != null) {
            transaction.begin();
        }

        session.saveOrUpdate(object);

        if (transaction != null) {
            transaction.commit();
        }

        session.close();
    }

    public <T> void delete(T object) {
        if (transaction != null) {
            transaction.begin();
        }

        session.delete(object);

        if (transaction != null) {
            transaction.commit();
        }

        session.close();
    }

    private <T> TypedQuery<T> prepareQuery(Class<T> type) {
        TypedQuery<T> typedQuery = session.createQuery(query, type);

        for (Map.Entry<String, Object> parameter : parameters.entrySet()) {
            typedQuery.setParameter(parameter.getKey(), parameter.getValue());
        }

        return typedQuery;
    }
}
