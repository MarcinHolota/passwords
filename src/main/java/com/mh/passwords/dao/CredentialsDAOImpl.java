package com.mh.passwords.dao;

import com.mh.passwords.facade.QueryFacade;
import com.mh.passwords.model.Credentials;
import com.mh.passwords.model.User;

import java.util.List;
import java.util.Optional;

public class CredentialsDAOImpl implements CredentialsDAO {

    @Override
    public void saveOrUpdate(Credentials credentials) {
        QueryFacade.createQuery()
                .withTransaction()
                .saveOrUpdate(credentials);
    }

    @Override
    public List<Credentials> getCredentialsByUser(User user) {
        return QueryFacade.createQuery()
                .withQuery("from Credentials where user = :user")
                .withParameter("user", user)
                .getList(Credentials.class);
    }

    @Override
    public Optional<Credentials> getCredentialsByUserAndWebsite(User user, String website) {
        return QueryFacade.createQuery()
                .withQuery("from Credentials where user = :user and website = :website")
                .withParameter("user", user)
                .withParameter("website", website)
                .getSingleResult(Credentials.class);
    }

    @Override
    public void delete(Credentials credentials) {
        QueryFacade.createQuery()
                .withTransaction()
                .delete(credentials);
    }
}
