package com.mh.passwords.dao;

import com.mh.passwords.model.User;

import java.util.Optional;

public interface UserDAO {
    void saveOrUpdate(User user);

    Optional<User> getUserByUsername(String username);
}
