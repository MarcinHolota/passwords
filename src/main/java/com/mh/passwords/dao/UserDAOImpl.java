package com.mh.passwords.dao;

import com.mh.passwords.facade.QueryFacade;
import com.mh.passwords.model.User;

import java.util.Optional;

public class UserDAOImpl implements UserDAO {

    public void saveOrUpdate(User user) {
        QueryFacade.createQuery()
                .withTransaction()
                .saveOrUpdate(user);
    }

    @Override
    public Optional<User> getUserByUsername(String username) {
        return QueryFacade.createQuery()
                .withQuery("from User where username = :username")
                .withParameter("username", username)
                .getSingleResult(User.class);
    }
}
