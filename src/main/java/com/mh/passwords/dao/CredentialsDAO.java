package com.mh.passwords.dao;

import com.mh.passwords.model.Credentials;
import com.mh.passwords.model.User;

import java.util.List;
import java.util.Optional;

public interface CredentialsDAO {

    void saveOrUpdate(Credentials credentials);

    List<Credentials> getCredentialsByUser(User user);

    Optional<Credentials> getCredentialsByUserAndWebsite(User user, String website);

    void delete(Credentials credentials);

}
