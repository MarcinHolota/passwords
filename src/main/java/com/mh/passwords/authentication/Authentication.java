package com.mh.passwords.authentication;

import com.mh.passwords.dao.UserDAO;
import com.mh.passwords.dao.UserDAOImpl;
import com.mh.passwords.model.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Optional;

public class Authentication {
    private static Authentication instance;
    private User authenticatedUser;
    private boolean authenticated;
    private final UserDAO userDAO;

    public static Authentication getInstance() {
        if (instance == null) {
            instance = new Authentication();
        }

        return instance;
    }

    private Authentication() {
        this.userDAO = new UserDAOImpl();
    }

    public boolean authenticate(String username, String rawPassword) throws AuthenticationException {
        if (authenticated)
            throw new AuthenticationException("User is already logged.");

        Optional<User> userOptional = userDAO.getUserByUsername(username);

        userOptional
                .filter(user -> new BCryptPasswordEncoder().matches(rawPassword, user.getPassword()))
                .ifPresent(user -> {
                    authenticatedUser = user;
                    authenticated = true;
                });

        return authenticated;
    }

    public void registerAndAuthenticate(User user) throws AuthenticationException {
        if (authenticated)
            throw new AuthenticationException("User is already logged.");

        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));

        userDAO.saveOrUpdate(user);

        authenticatedUser = user;
        authenticated = true;
    }

    public void unauthenticate() throws AuthenticationException {
        if (!authenticated)
            throw new AuthenticationException("User was not logged.");

        authenticatedUser = null;
        authenticated = false;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public User getAuthenticatedUser() throws AuthenticationException {
        if (!authenticated)
            throw new AuthenticationException("User is not authenticated.");

        return authenticatedUser;
    }
}
