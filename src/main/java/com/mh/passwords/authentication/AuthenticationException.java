package com.mh.passwords.authentication;

public class AuthenticationException extends Exception {

    public AuthenticationException(String message) {
        super(message);
    }

}
