package com.mh.passwords.view;

import com.mh.passwords.controller.LoginViewController;
import com.mh.passwords.controller.ViewManager;
import com.mh.passwords.model.Response;

import java.util.Scanner;

public class LoginView extends AuthenticationView {

    private final LoginViewController loginViewController;

    public LoginView(ViewManager viewManager, LoginViewController loginViewController) {
        super(viewManager);
        this.loginViewController = loginViewController;
    }

    @Override
    public void display() {
        Scanner scanner = new Scanner(System.in);

        boolean isAuthenticated;
        Response tryAgainResponse = null;

        do {
            System.out.println("Enter your credentials");
            System.out.print("username: ");
            String username = scanner.nextLine();
            System.out.print("password: ");
            String password = scanner.nextLine();

            isAuthenticated = loginViewController.tryToAuthenticate(username, password);

            if (!isAuthenticated) {
                do {
                    System.out.print("Do you want to try again?(YES/NO): ");
                    tryAgainResponse = loginViewController.tryAgain(scanner.nextLine());
                } while (tryAgainResponse.getStatus() == Response.Status.FAILED);
            }

        } while (!isAuthenticated && tryAgainResponse.getStatus() != Response.Status.GO_BACK);
    }
}
