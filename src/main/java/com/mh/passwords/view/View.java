package com.mh.passwords.view;

import com.mh.passwords.controller.ViewManager;

public abstract class View {

    protected final ViewManager viewManager;

    public View(ViewManager viewManager) {
        this.viewManager = viewManager;
    }

    public abstract void display();

}
