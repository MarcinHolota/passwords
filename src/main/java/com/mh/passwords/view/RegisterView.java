package com.mh.passwords.view;

import com.mh.passwords.controller.RegisterViewController;
import com.mh.passwords.controller.ViewManager;
import com.mh.passwords.model.Response;

import java.util.Scanner;

public class RegisterView extends AuthenticationView {

    private final RegisterViewController registerViewController;

    public RegisterView(ViewManager viewManager, RegisterViewController registerViewController) {
        super(viewManager);
        this.registerViewController = registerViewController;
    }

    @Override
    public void display() {
        Scanner scanner = new Scanner(System.in);

        Response responseRegistration;
        Response responseTryAgain = null;

        do {
            System.out.println("Register your account");
            System.out.print("username: ");
            String username = scanner.nextLine();
            System.out.print("password: ");
            String password = scanner.nextLine();
            System.out.print("confirm your password: ");
            String confirmedPassword = scanner.nextLine();
            responseRegistration = registerViewController.handleRegistration(username, password, confirmedPassword);
            System.out.println(responseRegistration);

            if (responseRegistration.getStatus() != Response.Status.OK) {
                do {
                    System.out.print("Do you want to try again?(YES/NO): ");
                    responseTryAgain = registerViewController.tryAgain(scanner.nextLine());
                } while (responseTryAgain.getStatus() == Response.Status.FAILED);
            }

        } while (responseRegistration.getStatus() != Response.Status.OK
                && responseTryAgain.getStatus() != Response.Status.GO_BACK);
    }
}
