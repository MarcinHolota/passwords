package com.mh.passwords.view;

import com.mh.passwords.authentication.Authentication;
import com.mh.passwords.controller.MainViewController;
import com.mh.passwords.controller.ViewManager;
import com.mh.passwords.model.Response;
import com.mh.passwords.state.AuthenticatedState;
import com.mh.passwords.state.RandomUserAuthenticatedState;
import com.mh.passwords.state.UserAuthenticatedState;

import java.util.Scanner;

public class MainView extends View {

    private final MainViewController mainViewController;
    private AuthenticatedState authenticatedState;

    public MainView(ViewManager viewManager, MainViewController mainViewController) {
        super(viewManager);
        this.mainViewController = mainViewController;
        this.authenticatedState = Authentication.getInstance().isAuthenticated() ? new UserAuthenticatedState() : new RandomUserAuthenticatedState();
    }

    @Override
    public void display() {
        authenticatedState.greeting();

        Response response;
        Scanner scanner = new Scanner(System.in);

        do {
            String request = scanner.nextLine();
            response = mainViewController.handleRequest(request);
            System.out.println(response);
        } while (response.getStatus() != Response.Status.EXIT
                && response.getStatus() != Response.Status.LOGOUT
                && response.getStatus() != Response.Status.GO_BACK
        );
    }
}
