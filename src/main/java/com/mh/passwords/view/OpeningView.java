package com.mh.passwords.view;

import com.mh.passwords.controller.OpeningViewController;
import com.mh.passwords.controller.ViewManager;

import java.util.Scanner;

public class OpeningView extends View {

    final private OpeningViewController openingViewController;

    public OpeningView(ViewManager viewManager, OpeningViewController openingViewController) {
        super(viewManager);
        this.openingViewController = openingViewController;
    }

    @Override
    public void display() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("///////////////");
        System.out.println("// PASSWORDS //");
        System.out.println("///////////////");
        System.out.println();

        do {
            System.out.println("Do you want to: ");
            System.out.println("1. login");
            System.out.println("2. register");
            System.out.println("3. use without account");
            System.out.println("Or use command like \"help\"");
            System.out.print("Enter value: ");
        } while (!openingViewController.handleAccountQuestionAnswer(scanner.nextLine()));
    }

}
