package com.mh.passwords.service;

import com.mh.passwords.model.PasswordConfiguration;

public interface PasswordGenerator {
    String generatePassword(PasswordConfiguration passwordConfiguration);
}
