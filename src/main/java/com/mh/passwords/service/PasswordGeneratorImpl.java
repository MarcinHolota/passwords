package com.mh.passwords.service;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.mh.passwords.model.PasswordConfiguration;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;

public class PasswordGeneratorImpl implements PasswordGenerator {

    private static final String lowerCaseCharacters = "abcdefghijklmnopqrstuvwxyz";
    private static final String upperCaseCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String digits = "1234567890";
    private static final String specialCharacters = "!@#$%&*()_+-=[]|,./?><";
    private static final Map<PasswordConfiguration.PasswordLength, Range> lengthMap =
            ImmutableMap.<PasswordConfiguration.PasswordLength, Range>builder()
                    .put(PasswordConfiguration.PasswordLength.SHORT, new Range(8, 17))
                    .put(PasswordConfiguration.PasswordLength.MEDIUM, new Range(17, 33))
                    .put(PasswordConfiguration.PasswordLength.LONG, new Range(33, 64))
                    .build();

    @Override
    public String generatePassword(PasswordConfiguration passwordConfiguration) {
        ImmutableList.Builder<String> builder = ImmutableList.builder();

        if (passwordConfiguration.isLowerCaseCharacters())
            builder.add(lowerCaseCharacters);
        if (passwordConfiguration.isUpperCaseCharacters())
            builder.add(upperCaseCharacters);
        if (passwordConfiguration.isDigits())
            builder.add(digits);
        if (passwordConfiguration.isSpecialCharacters())
            builder.add(specialCharacters);

        List<String> setsOfCharacters = builder.build();

        Random random = new Random();
        final int passwordLength = lengthMap.get(passwordConfiguration.getLength()).getRandomValue(random);

        return Stream
                .generate(() -> {
                    String set = setsOfCharacters.get(random.nextInt(setsOfCharacters.size()));
                    char character = set.charAt(random.nextInt(set.length()));
                    return String.valueOf(character);
                }).limit(passwordLength)
                .collect(joining());
    }

    @Getter
    @AllArgsConstructor
    private static class Range {
        private int begin;
        private int end;

        public int getRandomValue(Random random) {
            return begin + random.nextInt(end - begin);
        }
    }
}
