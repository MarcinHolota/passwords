package com.mh.passwords.state;

public class RandomUserAuthenticatedState implements AuthenticatedState {
    @Override
    public void greeting() {
        System.out.println("Hello User!");
        System.out.println("Consider to sign up to get all features!");
    }
}
