package com.mh.passwords.state;

import com.mh.passwords.authentication.Authentication;
import com.mh.passwords.authentication.AuthenticationException;
import com.mh.passwords.model.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserAuthenticatedState implements AuthenticatedState {

    @Override
    public void greeting() {
        try {
            User authenticatedUser = Authentication.getInstance().getAuthenticatedUser();
            System.out.println("Hello " + authenticatedUser.getUsername() + "!");
        } catch (AuthenticationException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
    }
}
