package com.mh.passwords.model;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Response {
    private final String message;
    private final Status status;

    public enum Status {
        REQUEST_NOT_RECOGNIZED,
        OK,
        EXIT,
        HELP,
        FAILED,
        LOGOUT,
        GO_BACK,
        ERROR
    }

    @Override
    public String toString() {
        return "Response status: " + status.name() + "\n" +
                "Response content: {\n" + message + "\n}\n";
    }
}
