package com.mh.passwords.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class PasswordConfiguration {
    private PasswordLength length;
    private boolean lowerCaseCharacters;
    private boolean upperCaseCharacters;
    private boolean digits;
    private boolean specialCharacters;

    public enum PasswordLength {
        SHORT("s"),
        MEDIUM("m"),
        LONG("l");

        private final String value;

        PasswordLength(final String value) {
            this.value = value;
        }

        public static PasswordLength getEnumOject(String value) {
            switch (value) {
                case "s":
                    return SHORT;
                case "m":
                    return MEDIUM;
                case "l":
                    return LONG;
                default:
                    throw new IllegalArgumentException();
            }
        }
    }

    public static PasswordConfigurationBuilder builder() {
        return new PasswordConfigurationBuilder();
    }

    public static class PasswordConfigurationBuilder {
        private PasswordLength length;
        private boolean lowerCaseCharacters;
        private boolean upperCaseCharacters;
        private boolean digits;
        private boolean specialCharacters;

        public PasswordConfigurationBuilder length(PasswordLength length) {
            this.length = length;
            return this;
        }

        public PasswordConfigurationBuilder useLowerCaseCharacters(boolean lowerCaseCharacters) {
            this.lowerCaseCharacters = lowerCaseCharacters;
            return this;
        }

        public PasswordConfigurationBuilder useUpperCaseCharacters(boolean upperCaseCharacters) {
            this.upperCaseCharacters = upperCaseCharacters;
            return this;
        }

        public PasswordConfigurationBuilder useDigits(boolean digits) {
            this.digits = digits;
            return this;
        }

        public PasswordConfigurationBuilder useSpecialCharacters(boolean specialCharacters) {
            this.specialCharacters = specialCharacters;
            return this;
        }

        public PasswordConfiguration build() {
            return new PasswordConfiguration(length, lowerCaseCharacters, upperCaseCharacters, digits, specialCharacters);
        }
    }
}
