package com.mh.passwords.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class Credentials {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @JoinColumn(referencedColumnName = "username")
    @ManyToOne(fetch = FetchType.EAGER)
    private User user;
    private String website;
    private String login;
    private String password;

    @Override
    public String toString() {
        return "website: " + website + "\n" +
                "login: " + login + "\n" +
                "password: " + password;
    }
}
